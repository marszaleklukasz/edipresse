<?php

namespace Marszaleklukasz\Task2;

class Post
{
    /**
     * @var int
     */
    private int $postId;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $post;

    /**
     * @var int
     */
    private int $userId;

    /**
     * @var int
     */
    private int $tagId;

    public function getPostId(): int
    {
        return $this->postId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPost(): string
    {
        return $this->post;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getTagId(): int
    {
        return $this->tagId;
    }

    public function setPostId(int $postId): void
    {
        $this->postId = $postId;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setPost(string $post): void
    {
        $this->post = $post;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function setTagId(int $tagId): void
    {
        $this->tagId = $tagId;
    }

    /**
     * @param array $filters
     *
     * The method returns data in JSON format
     * @return string
     */
    public function getPosts(array $filters = null): array
    {
        $result = [];
        // TODO - implementation

        return json_encode($result);
    }
}
