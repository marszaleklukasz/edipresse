<?php

namespace Marszaleklukasz\Task2;

class Tags
{
    /**
     * @var int
     */
    private int $tagId;

    /**
     * @var string
     */
    private string $tag;

    public function getTagId(): int
    {
        return $this->tagId;
    }

    public function getTag(): string
    {
        return $this->tag;
    }

    public function setTagId(int $tagId): void
    {
        $this->tagId = $tagId;
    }

    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    /**
     *
     * The method returns data in JSON format
     * @return string
     */
    public function getPosts(): string
    {
        $postObj = new Post();

        return $postObj->getPosts([
            'tagId' => $this->getTagId()
        ]);
    }
}
