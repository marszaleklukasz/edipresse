<?php

namespace Marszaleklukasz\Task2;

use Marszaleklukasz\Task2\Post;

class User
{
    /**
     * @var int
     */
    private int $userId;

    /**
     * @var string
     */
    private string $nick;

    /**
     * @var string
     */
    private string $name;

    function getUserId(): int
    {
        return $this->userId;
    }

    function getNick(): string
    {
        return $this->nick;
    }

    function getName(): string
    {
        return $this->name;
    }

    function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    function setNick(string $nick): void
    {
        $this->nick = $nick;
    }

    function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     *
     * The method returns data in JSON format
     * @return string
     */
    public function getPosts(): string
    {
        $postObj = new Post();

        return $postObj->getPosts([
            'userId' => $this->getUserId()
        ]);
    }
}
