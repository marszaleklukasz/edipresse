SELECT
	u.nick,
    u.name,
    t.tag,
    COUNT(p.post_id) AS count_posts
FROM 
	`Post` AS p
INNER JOIN
	`User` AS u
ON
	u.user_id = p.user_id
INNER JOIN
	`Tags` AS t
ON
	t.tag_id = p.tag_id
WHERE 
	1
GROUP BY 
	t.tag_id
HAVING 
	count_posts > 5
ORDER BY
	count_posts DESC
;